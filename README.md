Introduction to Version Control
===============================

Version control is the management of changes to files.

How does a version control system work?

![VCS image][VCS1]

Using a version control system you will have the following benefits:

- have a common repository for the files
- be able to get the latest changes easily
- group changes and commit them with a summary
- view history of changes, who made them and when
- merge different changes on the same file
- code review
- revert changes
- work on different versions of the application (branching)
- use continuous integration

Examples of VCSs:

SVN
---
* servers:
    - Apache Subversion server 
    - [VisualSVN]
* client: 
    - [TortoiseSVN]

Git
---
* servers
    - [git on the server]
    - [github]
    - [bitbucket]
    - [bonobo git server]
* clients 
    - git command line 
    - [git extentions]
    
TFS
---
* servers
    - Team Foundation Server
* client
    - TFS extention in Visual Studio
    
Merge tools
------------
- [winmerge]
- [KDiff3]
- Visual Studio diff

Hosting with continuous integration
------------------------------------
- [appharbor]
- [azure]

Resources
----------
- [Pro Git]
- [learnGitBranching]
- [Professional Team Foundation Server 2012]

[git on the server]:http://git-scm.com/book/en/Git-on-the-Server
[VisualSVN]:http://www.visualsvn.com/server/
[TortoiseSVN]:http://tortoisesvn.net/downloads.html
[github]:https://github.com/
[bitbucket]:https://bitbucket.org/
[git extentions]:http://code.google.com/p/gitextensions/downloads/list
[winmerge]:http://winmerge.org/downloads/
[KDiff3]:http://sourceforge.net/projects/kdiff3/files/kdiff3/
[appharbor]:https://appharbor.com/
[azure]:https://azure.microsoft.com/en-us/
[bonobo git server]:https://bonobogitserver.com/

[Pro Git]:http://git-scm.com/book/en/
[learnGitBranching]:http://pcottle.github.io/learnGitBranching/
[Professional Team Foundation Server 2012]:http://it-ebooks.info/book/1427/

[VCS1]:http://git-scm.com/figures/18333fig0102-tn.png
